import styles from "./page.module.css";

const getStrapi = async () => {
  const res = await fetch("http://localhost:3000/api/strapi")
  return res.json();
}

const getSendgrid = async () => {
  const res = await fetch("http://localhost:3000/api/sendgrid")
  return res.json();
}

export default async function Home() {
  const strapiData = await getStrapi();
  const sendgridData = await getSendgrid();

  return (
    <main className={styles.main}>
      <h1>{strapiData.data}</h1>
      <h1>{sendgridData.data}</h1>
    </main>
  );
}
